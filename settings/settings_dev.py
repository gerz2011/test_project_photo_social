"""Use this for development server"""

"""
linter does not recommend importing everything so it highlights as
a warning. Ignore this warning, everything works
"""
from .settings_base import *


ALLOWED_HOSTS += ['127.0.0.1', 'localhost']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

WSGI_APPLICATION = 'settings.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

from django.db import models
from django.db.models import Q


"""
1) работать со всеми одобренными фото каждой сущности (получить все фото столов);
2) работать со всеми одобренными фото отдельного типа (получить все фото из Албании - и самой Албании,  и столов, и стульев из неё);
3) работать со всеми неодобренными фото (чтобы вывести список для модераторов).
"""


class PictureManager(models.Manager):
    def approved_photo(self):
        return self.filter(approved='M').order_by('-date_pub')

    def not_approved_photo(self):
        return self.filter(approved='N').order_by('-date_pub')

    def all_photo(self):
        return self.select_related().all()

    def search_photos(self, query):
        return self.filter(
            Q(country__title__icontains=query) | Q(city__title__icontains=query) |
            Q(item__title__icontains=query) | Q(user__user__username__icontains=query)
        ).distinct()

    def self_country_photo(self, country_id):
        return self.filter(country_id=country_id)

    def self_city_photo(self, city_id):
        return self.filter(city_id=city_id)

    def self_item_photo(self, item_id):
        return self.filter(city_id=item_id)

    def country_related_photo(self, id):
        c_photo = self.raw('SELECT * FROM app_site_picture p WHERE (p.country_id = %s \
            OR p.city_id IN (SELECT c.id FROM app_site_city c WHERE c.country_id = %s) \
            OR p.item_id IN (SELECT i.id FROM app_site_item i WHERE i.city_id IN (SELECT c.id FROM app_site_city c WHERE c.country_id = %s)))', [id, id, id])
        return c_photo

    def city_related_photo(self, id):
        c_photo = self.raw('SELECT * FROM app_site_picture p WHERE (p.city_id = %s \
            OR p.item_id IN (SELECT i.id FROM app_site_item i WHERE i.city_id = %s))', [id, id])
        return c_photo

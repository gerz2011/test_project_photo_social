from django.contrib import admin

from .models import Profile, Item, City, Country, Picture


def make_approved(modeladmin, request, queryset):
    """action Admin panel approved photo"""
    queryset.update(approved='M')


def make_not_approved(modeladmin, request, queryset):
    """action Admin panel not approved photo"""
    queryset.update(approved='N')


make_approved.short_description = "Mark selected as (Moderated)"
make_not_approved.short_description = "Mark selected as (Not Moderated)"


class CityAdmin(admin.ModelAdmin):
    list_display = ('title', 'country')
    list_filter = ('country',)


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user',)
    search_fields = ('user',)


class ItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'city', 'user_profile')
    search_fields = ('title', 'city', 'user_profile')


class PictureAdmin(admin.ModelAdmin):
    raw_id_fields = ('user',)  # when more than a million users the select hangs
    list_display = ('id', 'name_file', 'tag_class', 'colored_approved', 'date_pub')
    list_filter = ('country', 'city',)
    list_display_links = ('name_file', 'colored_approved',)
    list_select_related = ['country', 'city', 'item', 'user']
    save_on_top = False
    view_on_site = True

    actions = [make_approved, make_not_approved]


admin.site.register(Country)
admin.site.register(City, CityAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Picture, PictureAdmin)

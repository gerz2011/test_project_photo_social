from django.urls import path, include
from rest_framework import routers

from .views import HomePageView, PictureDetailView, PictureViewSet, \
    CountryViewSet, CityViewSet, ProfileViewSet, ItemViewSet, UserViewSet

router = routers.DefaultRouter()
router.register('photos', PictureViewSet)
router.register('country', CountryViewSet)
router.register('item', ItemViewSet)
router.register('profile', ProfileViewSet)
router.register('city', CityViewSet)
router.register('user', UserViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('', HomePageView.as_view(), name='home_page'),
    path('<int:id>/', PictureDetailView.as_view(), name='picture_detail'),
]

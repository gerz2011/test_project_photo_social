from django.shortcuts import render
from django.views import View
from django.contrib.auth.models import User

from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters

from .models import Country, City, Profile, Item, Picture
from .serializers import PictureSerializer, CountrySerializer, CitySerializer,\
    ProfileSerializer, ItemSerializer, UserSerializer


class HomePageView(View):
    """
    main page view
    """

    def get(self, request):

        country_items = Country.objects.values_list('id', 'title', flat=False)
        city_items = City.objects.values_list('id', 'title', flat=False)
        item_items = Item.objects.values_list('id', 'title', flat=False)
        user_items = Profile.objects.values_list(
            'user_id__username', flat=True)
        context = {
            'country_items': country_items,
            'city_items': city_items,
            'item_items': item_items,
            'user_items': user_items
        }

        country_id = request.GET.get('country_title')
        city_id = request.GET.get('city_title')
        item_id = request.GET.get('item_title')
        related_check = request.GET.get('related')
        verified_check = request.GET.get('verified-select')
        query = request.GET.get('query')

        if request.GET.get('csrfmiddlewaretoken'):

            if verified_check == 'M':
                photos = Picture.objects.approved_photo()
            elif verified_check == 'N':
                photos = Picture.objects.not_approved_photo()
            elif verified_check == 'A':
                photos = Picture.objects.all_photo()

            # Country
            if bool(country_id):
                if related_check:
                    photos = Picture.objects.country_related_photo(country_id)
                else:
                    photos = Picture.objects.self_country_photo(country_id)

            # City
            if bool(city_id):
                if related_check:
                    photos = Picture.objects.city_related_photo(city_id)
                else:
                    photos = Picture.objects.self_city_photo(city_id)

            # Item
            if bool(item_id):
                photos = Picture.objects.self_item_photo(item_id)

            # Search
            if bool(query):
                print(1)
                photos = Picture.objects.search_photos(query)
                print(photos)

            context.update({'photos': photos})
            return render(request, 'index.html', context=context)

        photos = Picture.objects.all_photo()
        context.update({'photos': photos})
        return render(request, 'index.html', context=context)


class PictureDetailView(View):
    """
    picture detail page
    """

    def get(self, request, id):
        photo = Picture.objects.filter(id=id).first()
        return render(request, 'picture-detail.html', context={'photo': photo})


# ================= REST API =================

class PictureViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Picture to be viewed or edited.
    """
    queryset = Picture.objects.all()
    serializer_class = PictureSerializer
    filter_backends = [DjangoFilterBackend]
    filter_backends = [filters.SearchFilter]
    search_fields = ['country__title', 'city__title',
                     'item__title', 'user__user__username']


class CountryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Country to be viewed or edited.
    """
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    filter_backends = [DjangoFilterBackend]
    filter_backends = [filters.SearchFilter]
    search_fields = ['title']


class CityViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Country to be viewed or edited.
    """
    queryset = City.objects.all()
    serializer_class = CitySerializer
    filter_backends = [DjangoFilterBackend]
    filter_backends = [filters.SearchFilter]
    search_fields = ['title']


class ProfileViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Country to be viewed or edited.
    """
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    filter_backends = [DjangoFilterBackend]
    filter_backends = [filters.SearchFilter]
    search_fields = ['title']


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Country to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Country to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    filter_backends = [DjangoFilterBackend]
    filter_backends = [filters.SearchFilter]
    search_fields = ['title']

import os
from django.db import models
from django.shortcuts import reverse
from django.utils.html import format_html
from django.contrib.auth.models import User
from django.db.models import Q

from .managers import PictureManager


NOT_MODERATED = 'N'  # Не проверенная запись
MODERATED = 'M'  # прошла модерацию

MODERATION_CHOICES = (
    (NOT_MODERATED, 'Not Moderated'),
    (MODERATED, 'Moderated')
)


class Country(models.Model):
    title = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.title


class City(models.Model):
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE, related_name='city_country')
    title = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return '{} ({})'.format(self.title, self.country)


class Profile(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='profile_user')

    def __str__(self):
        return self.user.username


class Item(models.Model):
    city = models.ForeignKey(
        City, on_delete=models.CASCADE, related_name='item_city')
    user_profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name='item_user_profile')
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Picture(models.Model):
    image = models.ImageField(upload_to='uploads/% Y/% m/% d/')
    tumb = models.ImageField(upload_to='uploads/% Y/% m/% d/', blank=True)
    approved = models.CharField(
        max_length=100, choices=MODERATION_CHOICES, default='M')
    country = models.ForeignKey(Country, on_delete=models.CASCADE,
                                related_name='pic_country', blank=True, null=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE,
                             related_name='pic_city', blank=True, null=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE,
                             related_name='pic_item', blank=True, null=True)
    user = models.ForeignKey(Profile, on_delete=models.CASCADE,
                             related_name='pic_user', blank=True, null=True)
    date_pub = models.DateTimeField(auto_now_add=True)

    objects = PictureManager()

    def __str__(self):
        return os.path.basename(self.image.path)

    def name_file(self):
        return os.path.basename(self.image.path)

    def tag_class(self):
        list_title_objects = []
        if self.country:
            list_title_objects.append(self.country.title)
        if self.city:
            list_title_objects.append(self.city.title)
        if self.item:
            list_title_objects.append(self.item.title)
        if self.user:
            list_title_objects.append(self.user.user.username)

        return '[{}]'.format(', '.join(list_title_objects))


    def colored_approved(self):
        if self.approved == 'M':
            return format_html('<span style="color: green">Moderated</span>')
        return format_html('<span style="color: red">Not Moderated</span>')

    def get_absolute_url(self):
        return reverse('picture_detail', args=[self.id])
